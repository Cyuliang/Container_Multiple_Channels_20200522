TEMPLATE = subdirs

SUBDIRS +=\
    Main \
    ToUploadData\
    DataInterchange \
    DatabaseWrite \
    DatabaseRead \
    CaptureImages_underlying \
    TheDataAnalysis \
    InfraredLogic\
    Encryption\
    IdentifyImages\
    TheLicensePlate_WTY \
    TheLicensePlate_HCNET \
    CaptureImages_SDK_HCNET
